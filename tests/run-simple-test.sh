#!/bin/bash

set -ex
uname -a

make clean all

./add_person_cpp data 13 JustName emailAddress 012344444
OUT=$(./list_people.py data)

if [ "$OUT" != "13,JustName,emailAddress,012344444" ]; then
	echo "FAIL"
	exit 1
fi
make clean

exit 0
