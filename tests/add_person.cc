// Based on the examples from the protobuf release tarball

#include <ctime>
#include <fstream>
#include <google/protobuf/util/time_util.h>
#include <iostream>
#include <string>

#include "addressbook.pb.h"

using namespace std;

using google::protobuf::util::TimeUtil;

// Main function:  Reads the entire address book from a file,
//   adds one person based on user input, then writes it back out to the same
//   file.
int main(int argc, char* argv[]) {
  // Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against.
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  if (argc != 6) {
    cerr << "Usage:  " << argv[0] << " ADDRESS_BOOK_FILE ID NAME EMAIL PHONE" << endl;
    return -1;
  }

  tutorial::AddressBook address_book;

  fstream input(argv[1], ios::in | ios::binary);

  tutorial::Person* person = address_book.add_people();

  person->set_id(atoi(argv[2]));
  person->set_name(argv[3]);
  person->set_email(argv[4]);
  tutorial::Person::PhoneNumber* phone_number = person->add_phones();
  phone_number->set_number(argv[5]);
  phone_number->set_type(tutorial::Person::HOME);
  *person->mutable_last_updated() = TimeUtil::SecondsToTimestamp(42);

  {
    // Write the new address book back to disk.
    fstream output(argv[1], ios::out | ios::trunc | ios::binary);
    if (!address_book.SerializeToOstream(&output)) {
      cerr << "Failed to write address book." << endl;
      return -1;
    }
  }

  // Optional:  Delete all global objects allocated by libprotobuf.
  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}
